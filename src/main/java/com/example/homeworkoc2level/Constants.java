package com.example.homeworkoc2level;

public class Constants {

    public static final String DISTRICT = "district";
    public static final String CITY = "city";
    public static final String CO2LEVEL = "co2Level";
    public static final String TIME = "time";
    public static final String HALL = "hall_id";
    public static final String SENSOR = "sensor";
    public static final String CITY_HALL = "cityHall";
    public static final String COUNTRY = "country";

}
