package com.example.homeworkoc2level.controllers;

import com.example.homeworkoc2level.model.CityHall;
import com.example.homeworkoc2level.repositories.CityHallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityHallController {

    private final CityHallRepository cityHallRepository ;

    @Autowired
    public CityHallController(CityHallRepository cityHallRepository){
        this.cityHallRepository = cityHallRepository;
    }


    @PostMapping("/cityHall/addHall")
    public ResponseEntity<HttpStatus> addHall(@RequestBody CityHall cityHall){
        if (cityHall != null && cityHall.getCity() != null && cityHall.getCountry() != null ) {
            cityHallRepository.save(cityHall);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
