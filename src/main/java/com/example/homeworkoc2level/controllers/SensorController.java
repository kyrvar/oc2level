package com.example.homeworkoc2level.controllers;

import com.example.homeworkoc2level.model.Sensor;
import com.example.homeworkoc2level.repositories.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class SensorController {

    private final SensorRepository sensorRepository;

    @Autowired
    public SensorController(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    @GetMapping("/sensor/getSensor")
    public ResponseEntity<List<Sensor>> getSensorByHallIdDistrict(@RequestParam Integer hallId, @RequestParam String district){
        if(hallId != null && district != null) {
            List<Sensor> sensors = sensorRepository.findByHallIdAndDistrict(hallId,district);
            if (!sensors.isEmpty()) {
                return new ResponseEntity<>(sensors, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/sensor/addSensor")
    public ResponseEntity<HttpStatus> addSensor(@RequestBody Sensor sensor){
        if(checkForNull(sensor)) {
            sensor.setTime(new java.util.Date());
            sensorRepository.save(sensor);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private boolean checkForNull(Sensor sensor) {
        return sensor != null && sensor.getCity() != null && sensor.getCo2Level() != null
                && sensor.getDistrict() != null && sensor.getHallId() != null;
    }

}
