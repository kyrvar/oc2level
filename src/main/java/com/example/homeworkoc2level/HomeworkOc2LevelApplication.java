package com.example.homeworkoc2level;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkOc2LevelApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkOc2LevelApplication.class, args);
    }

}
