package com.example.homeworkoc2level.repositories;

import com.example.homeworkoc2level.model.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorRepository extends JpaRepository<Sensor,String> {
    List<Sensor> findByHallIdAndDistrict(Integer hallId, String district);
}
