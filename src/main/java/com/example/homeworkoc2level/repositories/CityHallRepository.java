package com.example.homeworkoc2level.repositories;

import com.example.homeworkoc2level.model.CityHall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityHallRepository extends JpaRepository<CityHall,String> {

}
