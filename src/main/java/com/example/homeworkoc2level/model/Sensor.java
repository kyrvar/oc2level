package com.example.homeworkoc2level.model;

import javax.persistence.*;
import java.util.Date;

import static com.example.homeworkoc2level.Constants.*;

@Entity
@Table(name = SENSOR)
public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column (name = DISTRICT)
    private String district;

    @Column (name = CITY)
    private String city;

    @Column (name = CO2LEVEL)
    private String co2Level;

    @Column (name = TIME)
    private Date time;

    @Column (name = HALL)
    private Integer hallId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCo2Level() {
        return co2Level;
    }

    public void setCo2Level(String co2Level) {
        this.co2Level = co2Level;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getHallId() {
        return hallId;
    }

    public void setHallId(Integer hallId) {
        this.hallId = hallId;
    }

}
