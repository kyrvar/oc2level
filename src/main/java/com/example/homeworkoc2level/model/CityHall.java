package com.example.homeworkoc2level.model;

import javax.persistence.*;

import static com.example.homeworkoc2level.Constants.*;

@Entity
@Table(name = CITY_HALL)
public class CityHall {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = CITY)
    private String city;

    @Column(name = COUNTRY)
    private String country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
