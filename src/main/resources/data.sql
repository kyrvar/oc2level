DROP TABLE IF EXISTS CITY_HALL;
DROP TABLE IF EXISTS SENSOR;


CREATE TABLE CITY_HALL (
                           id INT AUTO_INCREMENT PRIMARY KEY,
                           city VARCHAR(250) NOT NULL,
                           country VARCHAR(250) NOT NULL
);

CREATE TABLE SENSOR (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        district VARCHAR(250) NOT NULL,
                        city VARCHAR(250) NOT NULL,
                        co2Level VARCHAR (250) NOT NULL,
                        time TIMESTAMP NOT NULL,
                        hall_id INT NOT NULL,
                        FOREIGN KEY (hall_id) REFERENCES CITY_HALL(id)
);

INSERT INTO CITY_HALL (city, country) VALUES
('Barcelona','Spain'),
('Wien','Austria'),
('München','Germany');

INSERT INTO SENSOR (district, city, co2Level,time,hall_id) VALUES
('Gràcia', 'Barcelona', '21 to 29 mEq/L','2020-10-10T19:15:09.446+00:00',1),
('Eixample', 'Barcelona', '23 to 29 mEq/L','2020-5-16T19:15:09.446+00:00',1),
('Währing', 'Wien', '21 to 29 mEq/L','2020-10-10T19:15:09.446+00:00',2),
('Penzing', 'Wien', '23 to 29 mEq/L','2020-5-16T19:15:09.446+00:00',2),
('Maxvorstadt', 'München', '21 to 29 mEq/L','2020-10-10T19:15:09.446+00:00',3);



