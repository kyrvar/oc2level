package com.example.homeworkoc2level.controllers;

import com.example.homeworkoc2level.model.Sensor;
import com.example.homeworkoc2level.repositories.SensorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SensorControllerTest {

    @InjectMocks
    private SensorController sensorController;

    @Mock
    private SensorRepository sensorRepository;

    private final List<Sensor> sensors = new ArrayList<>();
    private Sensor sensor;

    @BeforeEach
    private void inti() {
        sensor = new Sensor();
        sensor.setCity("Wien");
        sensor.setDistrict("Währing");
        sensor.setCo2Level("21 to 29 mEq/L");
        sensor.setHallId(2);
        sensor.setTime(new java.util.Date() );
        sensors.add(sensor);
    }

    @Test
    public void testGetSensorByDistrictOkRequest(){
        when(sensorRepository.findByHallIdAndDistrict(2,"Währing")).thenReturn(sensors);
        ResponseEntity<List<Sensor>> responseEntity = sensorController.getSensorByHallIdDistrict(2,"Währing");
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testGetSensorByDistrictNotFound(){
        ResponseEntity<List<Sensor>> responseEntity = sensorController.getSensorByHallIdDistrict(null,"Währing");
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testAddSensorOk(){
        when(sensorRepository.save(sensor)).thenReturn(sensor);
        ResponseEntity<HttpStatus> responseEntity = sensorController.addSensor(sensor);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testAddSensorBadRequest(){
        sensor.setDistrict(null);
        ResponseEntity<HttpStatus> responseEntity = sensorController.addSensor(sensor);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }


}
