package com.example.homeworkoc2level.controllers;

import com.example.homeworkoc2level.model.CityHall;
import com.example.homeworkoc2level.repositories.CityHallRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CityHallControllerTest {

    private CityHall cityHall;
    @InjectMocks
    private CityHallController cityHallController;

    @Mock
    private CityHallRepository cityHallRepository;


    @BeforeEach
    private void init(){
        cityHall = new CityHall();
        cityHall.setId(1);
        cityHall.setCity("Wien");
        cityHall.setCountry("Austria");
    }

    @Test
    public void testAddHall() {
        when(cityHallRepository.save(cityHall)).thenReturn(cityHall);
        ResponseEntity<HttpStatus> responseEntity = cityHallController.addHall(cityHall);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testAddHallBadRequest() {
        cityHall.setCountry(null);
        ResponseEntity<HttpStatus> responseEntity = cityHallController.addHall(cityHall);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}
